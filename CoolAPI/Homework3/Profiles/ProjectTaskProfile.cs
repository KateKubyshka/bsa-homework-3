﻿using AutoMapper;
using Homework3.DTOs;
using Homework3.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.Profiles
{
    public class ProjectTaskProfile : Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTask, ProjectTaskDTO>();
            CreateMap<ProjectTaskDTO, ProjectTask>();
        }
    }
}
