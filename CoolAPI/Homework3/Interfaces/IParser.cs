﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.Interfaces
{
    interface IParser
    {
        IEnumerable<T> Parse<T>(string path);
    }
}
