﻿using Homework3.Entities;
using Homework3.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.Parsers
{
    public class Parser : IParser
    {
        public IEnumerable<T> Parse<T>(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(json);
        }
    }
}
