using Homework3.Entities;
using Homework3.Interfaces;
using Homework3.Parsers;
using Homework3.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSingleton<IGenericRepository<Project>,GenericRepository<Project>>();
            services.AddSingleton<IGenericRepository<ProjectTask>, GenericRepository<ProjectTask>>();
            services.AddSingleton<IGenericRepository<Team>, GenericRepository<Team>>();
            services.AddSingleton<IGenericRepository<User>, GenericRepository<User>>();
            services.AddTransient<IParser, Parser>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Homework3", Version = "v1" });
            });
            services.AddAutoMapper(typeof(Startup));
           
          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Homework3 v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            Seed(app);
        }

        private void Seed(IApplicationBuilder app)
        {
            var serviceProvider = app.ApplicationServices;
            var parser = serviceProvider.GetService<IParser>();
            var projects = serviceProvider.GetService<IGenericRepository<Project>>();
            var projectTasks = serviceProvider.GetService<IGenericRepository<ProjectTask>>();
            var teams = serviceProvider.GetService<IGenericRepository<Team>>();
            var users = serviceProvider.GetService<IGenericRepository<User>>();
            foreach (var item in parser.Parse<Project>(Configuration["Paths:Projects"]))
            {
                projects.Insert(item);
            }
            foreach (var item in parser.Parse<ProjectTask>(Configuration["Paths:ProjectsTasks"]))
            {
                projectTasks.Insert(item);
            }
            foreach (var item in parser.Parse<Team>(Configuration["Paths:Teams"]))
            {
                teams.Insert(item);
            }
            foreach (var item in parser.Parse<User>(Configuration["Paths:Users"]))
            {
                users.Insert(item);
            }
        }
    }
}
