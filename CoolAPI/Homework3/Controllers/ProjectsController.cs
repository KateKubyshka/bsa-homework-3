﻿using AutoMapper;
using Homework3.DTOs;
using Homework3.Entities;
using Homework3.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Homework3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IGenericRepository<Project> _repository;
        private readonly IMapper _mapper;

        public ProjectsController(IGenericRepository<Project> repository, IMapper mapper) => (_repository, _mapper) = (repository, mapper);

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<ProjectDTO>>(_repository.GetAll()));
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            try
            {
                return Ok(_mapper.Map<ProjectDTO>(_repository.GetById(id)));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO proj)
        {
            try
            {
                _repository.Update(_mapper.Map<Project>(proj));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPut]
        public ActionResult Put( [FromBody] ProjectDTO proj)
        {
            try
            {
                _repository.Insert(_mapper.Map<Project>(proj));
                return Ok();
            }
            catch (ArgumentException)
            {
                return Conflict();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
