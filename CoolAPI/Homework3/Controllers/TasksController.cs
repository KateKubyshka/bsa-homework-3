﻿using AutoMapper;
using Homework3.DTOs;
using Homework3.Entities;
using Homework3.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Homework3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IGenericRepository<ProjectTask> _repository;
        private readonly IMapper _mapper;

        public TasksController(IGenericRepository<ProjectTask> repository, IMapper mapper) => (_repository, _mapper) = (repository, mapper);

        [HttpGet]
        public ActionResult<IEnumerable<ProjectTaskDTO>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<ProjectTaskDTO>>(_repository.GetAll()));
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectTaskDTO> Get(int id)
        {
            try
            {
                return Ok(_mapper.Map<ProjectTaskDTO>(_repository.GetById(id)));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] ProjectTaskDTO task)
        {
            try
            {
                _repository.Update(_mapper.Map<ProjectTask>(task));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] ProjectTaskDTO task)
        {
            try
            {
                _repository.Insert(_mapper.Map<ProjectTask>(task));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
