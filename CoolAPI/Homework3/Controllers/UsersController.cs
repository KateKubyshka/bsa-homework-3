﻿using AutoMapper;
using Homework3.DTOs;
using Homework3.Entities;
using Homework3.Interfaces;
using Homework3.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace Homework3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IGenericRepository<User> _repository;
        private readonly IMapper _mapper;

        public UsersController(IGenericRepository<User> repository, IMapper mapper) => (_repository, _mapper) = (repository,mapper);

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<UserDTO>>(_repository.GetAll()));
      
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            try
            {
                return Ok(_mapper.Map<UserDTO>(_repository.GetById(id)));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] UserDTO user)
        {
            try
            {
                _repository.Update(_mapper.Map<User>(user));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] UserDTO user)
        {
            try
            {
                _repository.Insert(_mapper.Map<User>(user));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
           
        }
    }
}
