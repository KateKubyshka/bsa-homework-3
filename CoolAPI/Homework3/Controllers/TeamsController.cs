﻿using AutoMapper;
using Homework3.DTOs;
using Homework3.Entities;
using Homework3.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Homework3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IGenericRepository<Team> _repository;
        private readonly IMapper _mapper;

        public TeamsController(IGenericRepository<Team> repository, IMapper mapper) => (_repository, _mapper) = (repository, mapper);


        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<TeamDTO>>(_repository.GetAll()));
        }

       
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            try
            {
                return Ok(_mapper.Map<TeamDTO>(_repository.GetById(id)));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO team)
        {
            try
            {
                _repository.Update(_mapper.Map<Team>(team));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

   
        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO team)
        {
            try
            {
                _repository.Insert(_mapper.Map<Team>(team));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
