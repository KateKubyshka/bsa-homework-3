﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework3.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
