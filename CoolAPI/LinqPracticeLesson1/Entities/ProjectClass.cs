﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqPracticeLesson1
{
    public class ProjectClass
    {
        public Project Project { get; set; }
        public ProjectTask LongestTask { get; set; }
        public ProjectTask ShortestTask { get; set; }
        public int AmountOfMembers { get; set; }
    }
}
